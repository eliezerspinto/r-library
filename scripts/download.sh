#!/usr/bin/env bash

FILELIST=$(mktemp)
ROOTDIR=$(git rev-parse --show-toplevel)
OUTDIR="${ROOTDIR}/files"
BASEURL="http://git.psylab.info/r-library/raw/master"
ARIA2CARGS="--continue --max-connection-per-server=8 --split=8 --max-concurrent-downloads=8 --file-allocation=falloc --summary-interval=0 --log-level=error --check-integrity"
WGETARGS=""

if [ ! -d "${OUTDIR}" ]; then
    mkdir -p "${OUTDIR}"
fi

echo "Generate filelist"
cat "${ROOTDIR}"/bibs/*.bib | awk -F '[{}]' '/\.pdf|\.djvu|\.zip/ {gsub(/[,\ ]/, ""); print ($3"\n"$5)}' | awk 'NF' > ${FILELIST}    

echo "Download files from repo"
if hash aria2c 2> /dev/null; then
    echo "Use aria2c"
    aria2c ${ARIA2CARGS} --input-file=${FILELIST} --dir="${OUTDIR}"
else
    echo "Use wget"
    wget ${WGETARGS} --input-file=${FILELIST} --directory-prefix="${OUTDIR}"
fi

if  [ ! -z $? ]; then
    echo "Files successfully downloaded to ${OUTDIR} directory"
else
    echo "Some errors occurred while downloading files"
fi

rm ${FILELIST}
