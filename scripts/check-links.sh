#!/usr/bin/env bash

URLLIST=$(mktemp)
ROOTDIR=$(git rev-parse --show-toplevel)

echo "Generate loca URLs list"
cat "${ROOTDIR}"/bibs/*.bib | awk -F '[{}]' '/\.pdf|\.djvu|\.zip/ {gsub(/[,\ ]/, ""); print ($3"\n"$5)}' | awk 'NF' | sort -u > ${URLLIST}

while read LINE; do
  curl -o /dev/null --silent --head --write-out '%{http_code}' "$LINE"
  echo " $LINE"
done < ${URLLIST}

rm ${URLLIST}
